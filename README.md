 ### Spring-boot-monitor


类似于springbootadmin，提供网络应用监控，已实现基本框架，监控页面功能未实现。


### Reference Guide



    Version 0.1

### Getting Started


-     1.将spring-boot-monitor-0.1.jar添加到项目build-path中。
-     2.在springboot启动类上添加注解@EnableMonitorServer注解，即可启用Spring-boot-monitor。
-     3.在application.properties中添加spring.boot.monitor.username和spring.boot.monitor.password，登录页面使用这两个属性。

- 
### 访问路径

    http://ip:port/sbim


### 页面

    

   **登录页面：** 
  ![输入图片说明](https://git.oschina.net/uploads/images/2017/0930/100534_810412cc_688800.png "@FG$FP4FQ`HUDJ]L)7Y)}TK.png")

   **index页面** 
    ![输入图片说明](https://git.oschina.net/uploads/images/2017/0930/100627_d6b85e44_688800.png "P]][6]20}0[@XANZGZA{@VC.png")


### 使用说明

    目前实现了remove和Detail功能
     **remove:** 移除当前应用
     **Detail:** 详情页面将提供监控功能，暂未实现。

    