package com.cff.boot.monitor.store;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.cff.boot.monitor.model.AppInfo;

public class SimpleAppInfoStore {
	private final ConcurrentMap<String, AppInfo> map = new ConcurrentHashMap<>();

	public SimpleAppInfoStore() {
	}
	
	public void addApp(AppInfo appInfo){
		map.put(appInfo.getAppId(), appInfo);
	}
	
	public void remove(String appId){
		map.remove(appId);
	}
	
	public List<AppInfo> getAll(){
		List<AppInfo> lists = new ArrayList<AppInfo>(map.values()); 
		return lists;
	}
	
	public AppInfo getApp(String appId){
		return map.get(appId);
	}
}
