package com.cff.boot.monitor.config;

import org.springframework.context.annotation.DeferredImportSelector;
import org.springframework.core.type.AnnotationMetadata;

public class MonitorConfigImportSelector implements DeferredImportSelector {

	@Override
	public String[] selectImports(AnnotationMetadata importingClassMetadata) {
		return new String[] { MonitorConfiguration.class.getCanonicalName(),
				MonitorWebConfiguration.class.getCanonicalName()};
	}

}
