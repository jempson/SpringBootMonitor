package com.cff.boot.monitor.config;

import java.util.List;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.cff.boot.monitor.model.AppInfo;
import com.cff.boot.monitor.resolver.UrlFilteringResourceResolver;
import com.cff.boot.monitor.store.SimpleAppInfoStore;
import com.cff.boot.monitor.web.RestApiController;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@AutoConfigureAfter({MonitorConfiguration.class})
public class MonitorWebConfiguration extends WebMvcConfigurerAdapter
implements ApplicationContextAware{
	private final ApplicationEventPublisher publisher;
	private final ServerProperties server;
	private final ResourcePatternResolver resourcePatternResolver;
	private final MonitorProperties monitorProperties;
	private ApplicationContext applicationContext;

	public MonitorWebConfiguration(ApplicationEventPublisher publisher, ServerProperties server,
			ResourcePatternResolver resourcePatternResolver,
			MonitorProperties monitorProperties) {
		this.publisher = publisher;
		this.server = server;
		this.resourcePatternResolver = resourcePatternResolver;
		this.monitorProperties = monitorProperties;
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
	
	
	@Override
	public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
		if (!hasConverter(converters, MappingJackson2HttpMessageConverter.class)) {
			ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json()
					.applicationContext(this.applicationContext).build();
			converters.add(new MappingJackson2HttpMessageConverter(objectMapper));
		}
	}

	private boolean hasConverter(List<HttpMessageConverter<?>> converters,
			Class<? extends HttpMessageConverter<?>> clazz) {
		for (HttpMessageConverter<?> converter : converters) {
			if (clazz.isInstance(converter)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		System.out.println("路径：" + monitorProperties.getContextPath());
		registry.addResourceHandler(monitorProperties.getContextPath() + "/**")
				.addResourceLocations("classpath:/META-INF/spring-boot-monitor-ui/")
				.resourceChain(true)
				.addResolver(new UrlFilteringResourceResolver(".min"));
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		String contextPath = monitorProperties.getContextPath();
		if (StringUtils.hasText(contextPath)) {
			registry.addRedirectViewController(contextPath, server.getPath(contextPath) + "/");
		}
		registry.addViewController(contextPath + "/").setViewName("forward:login.html");
	}
	
	@Bean
    @ConditionalOnMissingBean
    public SimpleAppInfoStore appInfoStore() {
    	SimpleAppInfoStore simpleAppInfoStore = new SimpleAppInfoStore();
    	String appId = applicationContext.getId();
    	String appName = appId.substring(0, appId.indexOf(":"));
    	String appInfo = appId;
    	String appStatus = "UP";
    	AppInfo app = new AppInfo(appName,"",appInfo,appStatus);
    	simpleAppInfoStore.addApp(app);
        return simpleAppInfoStore;
    }
	
	@Bean
	@ConditionalOnMissingBean
	public RestApiController restApiController(SimpleAppInfoStore simpleAppInfoStore) {
		return new RestApiController(monitorProperties,simpleAppInfoStore);
	}
}
