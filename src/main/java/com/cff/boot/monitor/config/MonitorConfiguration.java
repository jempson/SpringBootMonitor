package com.cff.boot.monitor.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cff.boot.monitor.store.SimpleAppInfoStore;

@Configuration
@EnableConfigurationProperties(MonitorProperties.class)
public class MonitorConfiguration {
	private final MonitorProperties monitorProperties;

    public MonitorConfiguration(MonitorProperties monitorProperties) {
        this.monitorProperties = monitorProperties;
    }

}
