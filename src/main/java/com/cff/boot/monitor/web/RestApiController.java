package com.cff.boot.monitor.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cff.boot.monitor.config.MonitorProperties;
import com.cff.boot.monitor.model.AppInfo;
import com.cff.boot.monitor.model.SbimUser;
import com.cff.boot.monitor.store.SimpleAppInfoStore;

@WebController
@RequestMapping("/sbim/api")
@ResponseBody
public class RestApiController {
	private final MonitorProperties monitorProperties;
	private final SimpleAppInfoStore simpleAppInfoStore;
	private String contextPath = "/";
	public RestApiController(MonitorProperties monitorProperties, SimpleAppInfoStore simpleAppInfoStore) {
		this.monitorProperties = monitorProperties;
		this.simpleAppInfoStore = simpleAppInfoStore;
		contextPath = monitorProperties.getContextPath();
	}
	
	@RequestMapping(value= "/login", method=RequestMethod.POST)
    public String login(@RequestParam String loginUsername, @RequestParam String loginPassword, HttpServletRequest req) {
		SbimUser sbimUser = new SbimUser();
		sbimUser.setLoginUsername(loginUsername);
		sbimUser.setLoginPassword(loginPassword);
		if (monitorProperties.getUsername().equals(sbimUser.getLoginUsername())
				&& monitorProperties.getPassword().equals(sbimUser.getLoginPassword())){
			req.getSession().setAttribute("sbimUser", sbimUser);
	        return "success";
		} else {
			return "failed";
		}
		
    }
	
	@RequestMapping(value= "/appInfo", method=RequestMethod.POST)
    public List<AppInfo> appInfo(HttpServletRequest req) {
		SbimUser sbimUser = (SbimUser) req.getSession().getAttribute("sbimUser");
		if(sbimUser == null){
			return null;
		}
		
		List<AppInfo> lists = simpleAppInfoStore.getAll();
		return lists;
    }
	
	@RequestMapping(value= "/appRemove", method=RequestMethod.POST)
    public List<AppInfo> appRemove(HttpServletRequest req) {
		SbimUser sbimUser = (SbimUser) req.getSession().getAttribute("sbimUser");
		if(sbimUser == null){
			return null;
		}
		String appName = req.getParameter("appName");
		simpleAppInfoStore.remove(appName);
		List<AppInfo> lists = simpleAppInfoStore.getAll();
		return lists;
    }
}
