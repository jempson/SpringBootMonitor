package com.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import com.cff.boot.monitor.config.EnableMonitorServer;

import de.codecentric.boot.admin.config.EnableAdminServer;

@EnableCaching
@SpringBootApplication
@EnableAdminServer
@EnableMonitorServer
public class AccountApplication {
	public static void main(String[] args) {
        SpringApplication.run(AccountApplication.class, args);
    }
}
