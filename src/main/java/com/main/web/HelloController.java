package com.main.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
    @RequestMapping("/")
    public String index(Model model) {
    	model.addAttribute("hello", "欢迎使用最牛逼的系统");
        return "index";
    }
}
