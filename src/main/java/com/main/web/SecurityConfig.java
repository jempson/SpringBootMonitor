package com.main.web;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration  
public class SecurityConfig extends WebSecurityConfigurerAdapter {  
    @Override  
    protected void configure(HttpSecurity http) throws Exception {  
        http.authorizeRequests().antMatchers("/admin/**").authenticated();  
        http.authorizeRequests().antMatchers("/develop/**").authenticated();  
        http.formLogin();
        http.csrf().disable(); 
        // Enable so that the clients can authenticate via HTTP basic for registering  
        http.httpBasic(); 
    	
        return; 
    } 
    
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {  
        auth  
            .inMemoryAuthentication()  
            .withUser("admin").password("123456").roles("USER");  
    }
}  